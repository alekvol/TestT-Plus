﻿namespace WorkingWithAnArray
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1) ввод размера массива - n (целое число до 400);
            Console.Write("Введите размер массива (целое число до 400): ");
            if (int.TryParse(Console.ReadLine(), out int n) && n <= 400 && n >= 2)
            {
                var ls = new int[n];

                // 2) ввод массива ls целых чисел размером n (можно сгенерировать рандомом, числа от -1000 до 1000);
                var rand = new Random();
                for (int i = 0; i < n; i++)
                {
                    ls[i] = rand.Next(-1000,1000);
                }
                Console.WriteLine("Массив заполнен рандомными числами от -1000 до 1000");

                // 3) отсортировать массив в порядке возрастания чисел, вывести на экран;
                Console.WriteLine("Вывод отсортированного массива чисел: ");
                Array.Sort(ls);
                foreach (var i in ls)
                {
                    Console.Write(i + " ");
                }
                Console.WriteLine();

                // 4) удалить слева направо каждое нечетное по индексу число; считаем, что индексация начинается с единицы;
                // 5) из результирующего массива справа налево удалить каждое нечетное по индексу число;
                // 6) из результирующего массива удалить слева направо каждое нечетное по индексу число, затем справа налево;
                // 7) чередовать удаления, пока не останется одно число;

                FirstMethod(ref ls); // Начинаем проход слева-направо
                bool mode = true; // режим прохода true = справо-налево, false = слева-направо
                while (ls.Length != 1)
                {
                    if (ls.Length % 2 == 0 && mode == true) 
                    {
                        // если у нас попадается массив с кол-вом четных элементов,
                        //  то последнее число всегда будет на четной позиции, проходимся по массиву от обратного
                        SecondMethod(ref ls);
                    }
                    else
                    {
                        // для нечетного массива без разницы в какую сторону проходиться
                        // для четного  слева направо первый нечетный всегда, а конечный четный
                        FirstMethod(ref ls);
                    }
                    mode = !mode;
                }

                Console.WriteLine(ls[0]);

                void FirstMethod(ref int[] ls)
                {
                    int s = 0;
                    for (int i = 0; i < ls.Length; i++)
                    {
                        if (i % 2 != 0)
                        {
                            ls[s] = ls[i];
                            s++;
                        }
                    }
                    Array.Resize(ref ls, s);
                }
                void SecondMethod(ref int[] ls)
                {
                    int s = 0;
                    for (int i = 0; i < ls.Length; i++)
                    {
                        if (i % 2 == 0)
                        {
                            ls[s] = ls[i];
                            s++;
                        }
                    }
                    Array.Resize(ref ls, s);
                }
            }
            else
            {
                Console.WriteLine("Ошибка! Либо вы ввели символ, либо число меньше 2!");
            }
            
        }
        
    }
}